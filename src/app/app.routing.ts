import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TodoComponent } from './todo-app/todo-app.component';
 
export const AppRoutes: any = [
    { path: "", component: HomeComponent },
    { path: "about", component: AboutComponent },
    {path: "todo-app", component: TodoComponent}
];
 
export const AppComponents: any = [
    HomeComponent,
    AboutComponent,
    TodoComponent
];